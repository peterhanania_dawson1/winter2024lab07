public class SimpleWar {
    public static void main (String[] args) {

        Deck deck = new Deck();
        deck.shuffle();

        int playerOnePoints = 0;
        int playerTwoPoints = 0;

        while (deck.length() > 0) {
            Card playerOne = deck.drawTopCard();
            Card playerTwo = deck.drawTopCard();

            System.out.println("Player 1 took: " + playerOne);
            System.out.println("Player 2 took: " + playerTwo);

            double playerOneScore = playerOne.calculateScore();
            double playerTwoScore = playerTwo.calculateScore();

            System.out.println("Player 1 score: " + playerOneScore);
            System.out.println("Player 2 score: " + playerTwoScore);

            if(playerOneScore > playerTwoScore) {
                playerOnePoints++;

                System.out.println("\n\nPlayer 1 won this round\n\n");
            } else if(playerTwoScore > playerOneScore) {
                playerTwoPoints++;

                System.out.println("\n\nPlayer 2 won this round\n\n");
            } else {
                System.out.println ("\n\nboth players are tied\n\n");
            }

        }

        System.out.println("Player 1 Points: " + playerOnePoints + "\nPlayer 2 Points: " + playerTwoPoints + "\n\n");

        if(playerOnePoints > playerTwoPoints) {
            System.out.println("Player 1 won this round with: " + playerOnePoints + " points");
        } else if(playerTwoPoints > playerOnePoints) {
            System.out.println("Player 2 won this round with: " + playerTwoPoints + " points");
        } else {
            System.out.println ("both players are tied with: " + playerOnePoints + " points");
        }
    }
}