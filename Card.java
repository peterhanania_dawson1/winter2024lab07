public class Card {

  private String suit;
  private String value;

  public Card(String suit, String value) {
    this.suit = suit;
    this.value = value;
  }

  public String getSuit() {
    return suit;
  }

  public String getValue() {
    return value;
  }

  public String toString() {
    return value + " of " + suit;
  }

  public double calculateScore() {
    double score = 0;

    switch (value) {
      case "2":
        score = 2.0;
        break;
      case "3":
        score = 3.0;
        break;
      case "4":
        score = 4.0;
        break;
      case "5":
        score = 5.0;
        break;
      case "6":
        score = 6.0;
        break;
      case "7":
        score = 7.0;
        break;
      case "8":
        score = 8.0;
        break;
      case "9":
        score = 9.0;
        break;
      case "10":
        score = 10.0;
        break;
      case "Jack":
        score = 11.0;
        break;
      case "Queen":
        score = 12.0;
        break;
      case "King":
        score = 13.0;
        break;
      case "Ace":
        score = 1.0;
        break;
      default:
        score = 0;
    }

    switch (suit) {
      case "Hearts":
        score += 0.4;
        break;
      case "Spades":
        score += 0.3;
        break;
      case "Diamonds":
        score += 0.2;
        break;
      case "Clubs":
        score += 0.1;
        break;
    }

    return score;
  }
}
