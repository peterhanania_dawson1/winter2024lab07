import java.util.Random;

public class Deck {

  private Card[] cards;
  private int numberOfCards;
  private Random rng = new Random();

  public Deck() {
    cards = new Card[52];
    numberOfCards = 52;

    String[] suits = { "Hearts", "Diamonds", "Clubs", "Spades" };

    int index = 0;
    for (String suit : suits) {
      for (int value = 1; value <= 13; value++) {
        String cardValue = String.valueOf(value);

        if (value == 1) {
          cardValue = "Ace";
        } else if (value == 11) {
          cardValue = "Jack";
        } else if (value == 12) {
          cardValue = "Queen";
        } else if (value == 13) {
          cardValue = "King";
        }

        cards[index] = new Card(suit, cardValue);
        index++;
      }
    }
  }

  public int length() {
    return this.numberOfCards;
  }

  public Card drawTopCard() {
    if(numberOfCards == 0) return null;
    numberOfCards--;
    return cards[numberOfCards];
  }

  public String toString() {
    String str = "";
    for (Card card : cards) {
      if (card != null) {
        str += card.toString() + '\n';
      }
    }
    return str;
  }

  public void shuffle() {
    for (int i = 0; i < cards.length; i++) {
      int randomIndex = i + rng.nextInt(cards.length - i);
      Card currentCard = cards[i];
      cards[i] = cards[randomIndex];
      cards[randomIndex] = currentCard;
    }
  }
}
